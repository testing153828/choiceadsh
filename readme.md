# Choice Solutions Dashboard

This is a dashboard to help Choice employees more easily accomplish their most frequent tasks. Primary features include:

* Quick aggregate summaries of API outputs on Home page
* Rich tables for displaying and searching Zabbix Tickets, ConnectWise Tickets, SLA Violations, and more.
* An interactive table for adding, editing, or deleting Skus for products
* A multi-step wizard for sales engineers to setup product configurations for customers

## Stack

* Web Framework: [React](https://react.dev)
* Webpacker: [Vite](https://vitejs.dev)
* UI Framework: [Material UI](https://mui.com)
* Programming Language: [JSX](https://react.dev/learn/writing-markup-with-jsx)
* Documentation
    * Builder: [JSDoc](https://jsdoc.app)
    * Deployment: [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/)
    * Theme: [clean-jsdoc-theme](https://github.com/ankitskvmdam/clean-jsdoc-theme)

## Contributors

* Nat Riddle (Intern Summer 2023)
* Alex Steward (Intern Summer 2022–2023)
