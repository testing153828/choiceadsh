const path = require("path")
const fs = require("fs")

const names_and_files = {
    "package.json exists": ["package.json"],
    "package-lock.json exists": ["package-lock.json"],
    "Vite configuration file exists": ["vite.config.js"],
    "Main HTML file exists": ["index.html"],
    "Main JSX file exists": ["src", "main.jsx"],
    "App component exists": ["src", "App.jsx"],
}

for (const [name, file] of Object.entries(names_and_files)) {
    test(name, () => {
        expect(fs.existsSync(path.join(__dirname, ...file))).toBeTruthy()
    })
}
