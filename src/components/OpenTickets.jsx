import { useState, useEffect } from "react";
import { DataGrid } from "@mui/x-data-grid";
import { Link } from "react-router-dom";
import { Box, Typography } from "@mui/material";
import ContextMenu from "./ContextMenu";
import "../css/component/core.css";

/**
 * Fetches open ConnectWise tickets and displays them in a DataGrid.
 * Supports a context (right-click) menu.
 * @param {Object} parameters
 * @param {boolean} parameters.isDashboard Boolean to specify if the DataGrid
 *    is being rendered on the home page of the dashboard. When this is truthy,
 *    the table will have shorter pagination.
 */
export default function OpenTickets() {
  const [rows, setRows] = useState([]);
  const [selectedTableData, setSelectedTableData] = useState([]);
  const [selectedRowIds, setSelectedRowIds] = useState([]);
  const [contextMenuPosition, setContextMenuPosition] = useState(null);
  const [isBundleFormOpen, setIsBundleFormOpen] = useState(false);
  const [parentTicketNumber, setParentTicketNumber] = useState("");

  // TODO: Handle the selection option (e.g., edit, delete, bundle)
  const handleOptionSelected = (option) => {
    console.debug("Selected option:", option);
    if (option === "Bundle") {
      setIsBundleFormOpen(true);
    }
    setContextMenuPosition(null); // Close the context menu
    console.debug("Is bundle form open?", isBundleFormOpen);
  };

  // TODO: Handle bundle form submission, perform the bundling operation with
  // the selected tickets and parent ticket number
  const handleBundleSubmit = () => {
    console.debug("Parent Ticket Number:", parentTicketNumber);
    // Clear the form and close it
    setParentTicketNumber("");
    setIsBundleFormOpen(false);
  };

  useEffect(() => {
    fetch("http://10.100.10.152:5000/cwapi/queries/cwOpenTickets")
      .then((response) => response.json())
      .then((response) => setRows(response));
  }, []);

  // These reflect the fields returned by the API. Make sure
  // to keep matched with the API, such as if a field is renamed.
  const columns = [
    {
      field: "SR_Service_RecID",
      headerName: "ID",
      width: 90,
      renderCell: (params) => (
        <Link to={`/${params.row.SR_Service_RecID}`}>{params.row.SR_Service_RecID}</Link>
      )
    },
    {
      field: "Summary",
      headerName: "Summary",
      width: 500,
    },
    {
      field: "Company_ID",
      headerName: "Company ID",
      width: 190,
    },
    {
      field: "Date_Entered_UTC",
      headerName: "Date Entered",
      width: 150,
      valueGetter: (params) => (
        new Intl.DateTimeFormat("en-US", {
          dateStyle: "short",
          timeStyle: "short",
          timeZone: "America/Chicago",
        }).format(
          new Date(params.row.Date_Entered_UTC)
        )
      )
    },
    {
      field: "Board_Name",
      headerName: "Board",
      width: 90,
    },
    {
      field: "Rec_Type",
      headerName: "Type",
      width: 90,
    },
    {
      field: "Status_Name",
      headerName: "Status",
      width: 200,
    },
    {
      field: "Contact_Full_Name",
      headerName: "Last Contact",
      width: 200,
    },
  ];

  // We have to manually implement multi-selection functionality with a custom
  // selection model because the proper implementation is only available in
  // MUI Data Grid Pro.
  const handleSelectionModelChange = (newSelection) => {
    setSelectedRowIds(newSelection.selectionModel || []); // Handle undefined selectionModel
    setSelectedTableData(
      (newSelection.selectionModel || []).map((selectedId) =>
        rows.find((row) => row.SR_Service_RecID === selectedId)
      )
    );
  };

  return (
    <Box>
      <Typography variant="h1" gutterBottom>Open Tickets</Typography>
      <Box
        onContextMenu={(event) => {
          event.preventDefault();
          setContextMenuPosition({ x: event.clientX, y: event.clientY });
        }}
      >
        <DataGrid
          autoHeight
          rows={rows}
          columns={columns}
          classes={"ticketlist"}
          initialState={{
            pagination: {
              paginationModel: {
                pageSize: 15,
              },
            },
          }}
          pageSizeOptions={[15, 30, 50, 100]}
          getRowId={(row) => row.SR_Service_RecID}
          checkboxSelection
          onSelectionModelChange={handleSelectionModelChange}
        />
        {contextMenuPosition && (
          <ContextMenu
            position={contextMenuPosition}
            onOptionSelected={handleOptionSelected}
          />
        )}
        {isBundleFormOpen && (
          <Box className="bundleForm">
            <input
              type="text"
              value={parentTicketNumber}
              onChange={(event) => setParentTicketNumber(event.target.value)}
              placeholder="Enter Parent Ticket Number"
            />
            <button onClick={handleBundleSubmit}>Bundle</button>
          </Box>
        )}
      </Box>
    </Box>
  );
}
