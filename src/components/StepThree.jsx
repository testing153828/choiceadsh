import { useState, useEffect } from "react";
import { Box, Divider, Stack, Typography } from "@mui/material";
import "localforage";

/**
 * This is abstracted out in case I want to change how it is implemented, for
 * instance using `<tr>`. Currently this uses a horizontal flexbox.
 * @param {Object} parameters Any unknown parameters will be added directly to
 *      the root component.
 * @param {Array} parameters.children The cells. Auto-populated by JSX.
 * @param {boolean} parameters.hidden An optional boolean. If true, the row is
 *      invisible. Defaults to false.
 */
function Row({ children, hidden = false, ...rest }) {
    return <Box display={hidden ? "none" : "flex"} width="100%" py={0.75}
        borderBottom="1px solid grey" borderColor="grey.900" {...rest}>{children}</Box>;
}

/**
 * This subclasses `Row`, and it is abstracted out in case I want to change how
 * `Row` is implemented, for instance using `<tr>`. Make sure to keep all
 * children of `HeaderRow`s consistent with other `Row`s.
 */
function HeaderRow({ children, ...rest }) {
    return <Row backgroundColor="#f5f5f5" fontSize={12} {...rest}>{children}</Row>;
}

/**
 * This is abstracted out in case I want to change how it is implemented, for
 * instance using `<td>`. Currently this is a flex child.
 * @param {Object} parameters Any unknown parameters will be added directly to
 *      the root component.
 * @param {number} parameters.size The relative width / column span of this cell.
 * @param {Array} parameters.children The cell contents. Auto-populated by JSX.
 */
function Cell({ size, children, ...rest }) {
    return <Box flex={size} px={1} {...rest}>{children}</Box>;
}

/**
 * This is only referenced once, so the only reason this is its own component
 * is because it contains a state variable and an effect that reflect the IndexedDB.
 * @param {Object} parameters 
 * @param {Object} parameters.sku A Sku row returned by the API.
 */
function SkuRow({ sku, quantity }) {
    return (
        <Row>
            <Cell size={2}>{sku.sku}</Cell>
            <Cell size={3}>
                {sku.description} /
                <em>{sku.hardCost ? "Hard" : "Soft"}</em>
            </Cell>
            <Cell size={1}>
                ${sku.unitPrice} /
                ${sku.unitCost}</Cell>
            <Cell size={1} color="steelblue">
                ${(sku.unitPrice * quantity).toFixed(2)} /
                <em>${(sku.unitCost * quantity).toFixed(2)}</em>
            </Cell>
            <Cell size={"0 0 75px"}>{quantity}</Cell>
        </Row>
    );
}

/**
 * Step Three is similar to Step Two, but it lists every Sku row that has a
 * nonzero value in the IndexedDB. This is done by iterating again through the
 * fetched list of Sku Groups and querying the IndexedDB for each one.
 * 
 * Note that it makes no distinction between the cache key being missing and
 * being present but set to 0. My assumption is that this is no problem because
 * a summary view should never need to display a product included 0 times. This
 * step is designed to be read-only; any changes to the included products should
 * be done by returning to Step Two. This gives the steps distinct purposes and
 * makes this step easier to implement.
 * @param {Object} parameters 
 * @param {Object} parameters.groups An object, with keys containing the names
 *      of the groups, and values containing arrays of every Sku row.
 */
export default function StepThree({ groups }) {
    const [customerCost, setCustomerCost] = useState(0);
    const [choiceCost, setChoiceCost] = useState(0);
    const [skuGroups, setSkuGroups] = useState([]);

    // This effect transforms the `groups` object, replacing every Sku row
    // with an two-item array, the first item being the original Sku row, and
    // the second being the currently configured value saved in the IndexedDB.
    // This uses `localforage.getItem` at the deepest nesting level of the loop,
    // which ripples through the logic and makes it all really complicated.
    // Any time an iterator method like `map` is used, it now needs to take an
    // `async` callable, and it needs to be wrapped in `await Promise.all(...)`.
    // And since the entire thing is async, that's why it needs to be an effect.

    // I know, disgusting.
    useEffect(() => {
        (async () => {
            let new_groups = Object.fromEntries(
                (await Promise.all(
                    Object.entries(groups).map(async ([name, skus]) => (
                        [name, (await Promise.all(
                            skus.map(async (sku) => (
                                [sku, await localforage.getItem(sku.sku)]
                            ))
                        )).filter(([sku, quantity]) => quantity)]
                    )))
                ).filter(([name, skus_quantities]) => skus_quantities.length)
            );
            setSkuGroups(new_groups);
            setCustomerCost(
                Object.values(new_groups).map((sku_quantities) =>
                    sku_quantities.map(([sku, quantity]) =>
                        quantity * sku.unitPrice
                    ).reduce((a, b) => a + b)
                ).reduce((a, b) => a + b, 0)
            );
            setChoiceCost(
                Object.values(new_groups).map((sku_quantities) =>
                    sku_quantities.map(([sku, quantity]) =>
                        quantity * sku.unitCost
                    ).reduce((a, b) => a + b)
                ).reduce((a, b) => a + b, 0)
            );
        })();
    }, []);
    return (
        <Stack>
            <Box sx={{ mb: 3 }}>
                {Object.entries(skuGroups).map(([name, skus_quantities]) => (
                    <Box key={name}>
                        <HeaderRow>
                            <Cell size={2}><b>{name}</b></Cell>
                            <Cell size={3}>Description / <em>Hard or Soft</em></Cell>
                            <Cell size={1}>Customer Price / <em>Choice Cost</em></Cell>
                            <Cell size={1}>Customer Total / <em>Choice Total</em></Cell>
                            <Cell size={"0 0 75px"}><em>Quantity</em></Cell>
                        </HeaderRow>
                        {skus_quantities.map(([sku, quantity]) => <SkuRow key={sku.sku} sku={sku} quantity={quantity} />)}
                    </Box>
                ))}
            </Box>
            <Divider sx={{ mb: 3 }} />
            <Box>
                <Typography variant="h1"><em>Customer Cost:</em> ${customerCost} &mdash; <em>Choice Cost: </em>${choiceCost}</Typography>
            </Box>
        </Stack>
    );
}