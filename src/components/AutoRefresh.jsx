import { useEffect } from "react";

/**
 * TODO. Utility for enabling periodic reloading. Does not update the entire
 * page, but rather calls a refresher function passed to it.
 * @param {Object} parameters 
 * @param {number} parameters.interval The number of seconds between reloads.
 * @param {CallableFunction} parameters.onRefresh The function to call to
 *    perform the reloading.
 */
export default function AutoRefresh({ interval, onRefresh }) {
  useEffect(() => {
    const timer = setInterval(() => {
      onRefresh();
    }, interval);

    return () => {
      clearInterval(timer);
    };
  }, [interval, onRefresh]);

  return null;
}
