import { useState, useEffect } from "react";
import { Box, Typography, Card, CardContent } from "@mui/material";
import { Link } from "react-router-dom";

/**
 * A few tiny cards at the top of the home page that numerically summarize
 * information from various APIs.
 */
export default function FeaturedInfo() {
  const [slaViolations, setSlaViolations] = useState([]);
  const [zabbixAlerts, setZabbixAlerts] = useState([]);
  const [xdrIncidents, setXdrIncidents] = useState([]);
  const [huntressIncidents, setHuntressIncidents] = useState([]);
  const [productionDown, setProductionDown] = useState([]);

  useEffect(() => {
    fetch("http://10.100.10.152:5000/cwapi/ticketCount/sla")
      .then((response) => response.json())
      .then((response) => setSlaViolations(response));
  }, []);
  useEffect(() => {
    fetch("http://10.100.10.152:5000/zabbix/sql/problemsCount")
      .then((response) => response.json())
      .then((response) => setZabbixAlerts(response));
  }, []);
  useEffect(() => {
    fetch("http://10.100.10.152:5000/xdr/incidents")
      .then((response) => response.json())
      .then((response) => setXdrIncidents(response));
  }, []);
  useEffect(() => {
    fetch("http://10.100.10.152:5000/huntress/incidents")
      .then((response) => response.json())
      .then((response) => setHuntressIncidents(response));
  }, []);
  useEffect(() => {
    fetch("http://10.100.10.152:5000/cwapi/ticketCount/productionDown")
      .then((response) => response.json())
      .then((response) => setProductionDown(response));
  }, []);

  return (
    <Box display="flex" gap={2}
      onContextMenu={(event) => {
        event.preventDefault();
        setContextMenuPosition({ x: event.clientX, y: event.clientY });
      }}>
      <Card sx={{ flexGrow: 1 }} variant="outlined">
        <CardContent>
          <Link to="/slaTickets">
            <Typography variant="h5" gutterBottom>SLA Violated</Typography>
            <Typography>{slaViolations.count}</Typography>
          </Link>
        </CardContent>
      </Card>
      <Card sx={{ flexGrow: 1 }} variant="outlined">
        <CardContent>
          <Link to="/zabbixProblems">
            <Typography variant="h5" gutterBottom>Zabbix Alerts</Typography>
            {zabbixAlerts.map((category) => (
              <Typography key={category.PName}>{category.PName}: {category.count}</Typography>
            ))}
          </Link>
        </CardContent>
      </Card>
      <Card sx={{ flexGrow: 1 }} variant="outlined">
        <CardContent>
          <Typography variant="h5" gutterBottom>XDR Incidents</Typography>
          <Typography>{xdrIncidents.length}</Typography>
        </CardContent>
      </Card>
      <Card sx={{ flexGrow: 1 }} variant="outlined">
        <CardContent>
          <Typography variant="h5" gutterBottom>Huntress Incidents</Typography>
          <Typography>{huntressIncidents.length}</Typography>
        </CardContent>
      </Card>
      <Card sx={{ flexGrow: 1 }} variant="outlined">
        <CardContent>
          <Link to="/cwSLA/sla">
            <Typography variant="h5" gutterBottom>Production Down</Typography>
            <Typography>{productionDown.count}</Typography>
          </Link>
        </CardContent>
      </Card>
    </Box>
  );
}