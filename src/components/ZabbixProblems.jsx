import { useState, useEffect } from "react";
import { DataGrid } from "@mui/x-data-grid";
import { Box, Typography } from "@mui/material";
import ContextMenu from "./ContextMenu";
import "../css/component/core.css";

/**
 * Fetches Zabbix problems and displays them in a DataGrid.
 * Supports a context (right-click) menu.
 * @param {Object} parameters
 * @param {boolean} parameters.isDashboard Boolean to specify if the DataGrid
 *    is being rendered on the home page of the dashboard. When this is truthy,
 *    the table will have shorter pagination.
 */
export default function ZabbixProblems({ isDashboard }) {
  const [rows, setRows] = useState([]);
  const [selectedRowIds, setSelectedRowIds] = useState([]);
  const [contextMenuPosition, setContextMenuPosition] = useState(null);
  const [showTicketPopup, setShowTicketPopup] = useState(false);
  const [selectedTicketData, setSelectedTicketData] = useState(null);

  // TODO: Handle other options
  const handleOptionSelected = (option) => {
    if (option === "OpenTicket" && selectedTicketData) {
      setShowTicketPopup(true);
    } else {
      setContextMenuPosition(null);
      setShowTicketPopup(false);
    }
  };

  useEffect(() => {
    fetch("http://10.100.10.152:5000/zabbix/sql/problems")
      .then((response) => response.json())
      .then((data) => setRows(data));
  }, []);

  // These reflect the fields returned by the API. Make sure
  // to keep matched with the API, such as if a field is renamed.
  const columns = [
    {
      field: "eventid",
      headerName: "Event ID",
      width: 90,
      renderCell: (params) => (
        <a href={`https://zmonitoring.choicecloud.com/zabbix/tr_events.php?triggerid=${params.row.triggerid}&eventid=${params.row.eventid}`} target="_blank" rel="noopener noreferrer">
          {params.row.eventid}
        </a>
      )
    },
    {
      field: "triggerid",
      headerName: "Trigger ID",
      width: 90,
    },
    {
      field: "problem_name",
      headerName: "Summary",
      width: isDashboard ? 300 : 550,
    },
    {
      field: "Company_ID",
      headerName: "Company",
      width: isDashboard ? 150 : 200,
    },
    {
      field: "host_name",
      headerName: "Host Name",
      width: 175,
    },
    {
      field: "date_time",
      headerName: "Date Entered",
      width: isDashboard ? 140 : 150,
      valueGetter: (params) => (
        new Intl.DateTimeFormat("en-US", {
          dateStyle: "short",
          timeStyle: "short",
          timeZone: "America/Chicago",
        }).format(
          new Date(params.row.date_time)
        )
      )
    },
    {
      field: "priority",
      headerName: "Priority",
      width: 125,
    },
    {
      field: "acknowledged",
      headerName: "Acknowledged",
      width: 125,
    },
  ];

  const handleSelectionModelChange = (newSelection) => {
    const selectionModel = newSelection?.selectionModel || [];
    setSelectedRowIds(selectionModel);
  };

  const handleContextMenu = (event, rowParams) => {
    event.preventDefault();
    const clickedRowElement = event.target.closest(".MuiDataGrid-row");
    if (clickedRowElement) {
      const clickedRowId = clickedRowElement.getAttribute("data-id");
      const selectedData = rows.find(
        (row) => String(row?.eventid) === clickedRowId
      );
      if (selectedData) {
        setSelectedTicketData(selectedData);
      } else {
        console.debug("Selected data not found.");
      }
    }
    setContextMenuPosition({ x: event.clientX, y: event.clientY });
  };

  const createTicketInConnectWise = () => {
    const connectWiseTicketData = {
      Company: selectedTicketData.Company_ID,
      initialDescription: selectedTicketData.item_name,
      summary: selectedTicketData.problem_name,

      // 5 maps to 7, 4 maps to 8, 3 or anything else maps to 10
      priority: { 5: 7, 4: 8 }[selectedTicketData.priority] || 10,
    };
    console.debug("connectWiseTicketData:", connectWiseTicketData);

    // API requires form-urlencoded for some reason
    fetch("http://10.100.10.152:5000/cwapi/ticket", {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: Object.keys(connectWiseTicketData)
        .map(key => encodeURIComponent(key) + "=" +
          encodeURIComponent(connectWiseTicketData[key]))
        .join("&")
    });
  };

  return (
    <Box>
      <Typography variant="h1" gutterBottom>Zabbix Problems</Typography>
      <Box onContextMenu={handleContextMenu}>
        <DataGrid
          autoHeight
          rows={rows}
          disableSelectionOnClick
          columns={columns}
          classes={"ticketlist"}
          initialState={{
            pagination: {
              paginationModel: {
                pageSize: 15,
              },
            },
            columns: {
              columnVisibilityModel: {
                triggerid: !isDashboard,
                host_name: !isDashboard,
                priority: !isDashboard,
                acknowledged: !isDashboard
              }
            }
          }}
          pageSizeOptions={[15, 30, 50, 100]}
          getRowId={(row) => row?.eventid}
          checkboxSelection
          onSelectionModelChange={handleSelectionModelChange}
        />
        {contextMenuPosition && (
          <ContextMenu
            position={contextMenuPosition}
            onOptionSelected={handleOptionSelected}
            selectedData={selectedTicketData}
          />
        )}
        {showTicketPopup && selectedTicketData && (
          <Box className="ticketPopup">
            <h2>Ticket Information</h2>
            <ul>
              <li><strong>Event ID:</strong> {selectedTicketData.eventid}</li>
              <li><strong>Trigger ID:</strong> {selectedTicketData.triggerid}</li>
              <li><strong>Company ID:</strong> {selectedTicketData.Company_ID}</li>
              <li><strong>Priority:</strong> {selectedTicketData.priority}</li>
              <li><strong>Item Name:</strong> {selectedTicketData.item_name}</li>
            </ul>
            <button onClick={createTicketInConnectWise}>Submit</button>
          </Box>
        )}
      </Box>
    </Box>
  );
}
