import { Link } from "react-router-dom";
import { Box } from "@mui/material";

/**
 * The banner at the top of every page of the dashboard. Ideally, this could
 * contain a dynamic heading that updates based on the page, possibly using `useContext`.
 */
export default function Topbar() {
  return (
    <Box px={4} py={2}>
      <Link to="/"><img src="logo_black.svg" alt="Choice Solutions Logo" height="50px" /></Link>
    </Box>
  );
}
